# README #

libpoker is a library for building applications based on the texas hold'em game. It is not complete yet, and there is no documentation.

### What is this repository for? ###

You can build your own applications ontop of the libpoker library. libpoker is fast and has a low memory footprint.

### How do I get set up? ###

Clone the repository and run "make" it should build the library (libpoker/lib/) and a driver for the program (bin/). The driver simulates poker games in order to find out the winning chance of a given hand.


The following example shows that a pair of 10s has about a 57% chance to win against AK offsuit

```
#!bash

$ bin/poker_static --players 2 --simulations 1000000 AsKd TsTh
```

```
Players: 2, threads: 1
Player 0 hand: 14s13d
Player 1 hand: 10s10h
Player 0 wins: 431167 (43.116700%)
Player 1 wins: 565519 (56.551900%)
Ties: 3314 (0.331400%)
```