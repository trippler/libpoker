#include <assert.h>
#include "hand.h"
#include "engine.h"
typedef struct{
		int value;
		Ruleresult (*rule_ptr)(Hand*);
}Rule;

Rule rules[] = 
{
	{STRAIGHTFLUSH, straightflush},
	{FOUR_OF_A_KIND, four_of_a_kind},
	{FULL_HOUSE, full_house},
	{FLUSH, flush},
	{STRAIGHT, straight},
	{THREE_OF_A_KIND, three_of_a_kind},
	{TWO_PAIR, two_pair},
	{ONE_PAIR, one_pair},
	{HIGH_CARD, high_card}
};

Handscore hand_score(Hand* h){
	Handscore score = {0, {false, hand_create()}};
	hand_sort(h);
	for(int n = 0; n < 9 && score.value == 0; ++n){
		Ruleresult r = rules[n].rule_ptr(h);
		if(r.matches){
			score.value = rules[n].value;
			score.result = r;
		}
	}
	return score;
}

int hands_compare(Hand* a, Hand* b){
	Handscore a_score = hand_score(a);
	Handscore b_score = hand_score(b);
	if(a_score.value != b_score.value)
		return a_score.value - b_score.value;
	assert(a_score.result.hand.count == b_score.result.hand.count);
	for(int n = 0; n < a_score.result.hand.count; ++n){
		int v = a_score.result.hand.cards[n].rank
				- b_score.result.hand.cards[n].rank;
		if(v != 0)
			return v;
	}
	return 0;
}
