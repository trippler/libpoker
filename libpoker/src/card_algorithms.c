#include "hand.h"

Hand get_cards_with_highest_suit(Hand* h){
	Hand ret = hand_create();
	int counts[4] = {0,0,0,0};
	for(int n = 0; n < h->count; ++n)
		counts[(int)h->cards[n].suit]++;
	int suit_index_pointer = 0;
	for(int n = 1; n < 4; ++n)
		if(counts[n] > counts[suit_index_pointer])
			suit_index_pointer = n;
	for(int n = 0; n < h->count; ++n)
		if(h->cards[n].suit == (Suit)suit_index_pointer)
			hand_card_add(&ret, h->cards[n]);
	return ret;
}
