#include <stdio.h>
#include <stdlib.h>
#include "hand.h"
#include "card.h"

Hand hand_create(){
	Hand h;
	h.count = 0;
	return h;
}
void hand_card_add(Hand* h, Card c){
	if(h->count <= 6)
		h->cards[h->count++] = c;
}
void hand_print(Hand* h){
	for(int n = 0; n < h->count; ++n){
		printf("%d%c", h->cards[n].rank, suit_to_str(h->cards[n].suit)[0]);
	}
}
void hand_sort(Hand* h){
	qsort(h->cards, h->count, sizeof(Card), cards_compare);
}

