#include <stdlib.h>
#include "deck.h"
Deck deck_create(){
	Deck d;
	int ind = 0;
	//Make all cards;
	for(int n = 2; n <= 14; ++n){
		for(int m = 0; m < 4; ++m){
			d.cards[ind++] = (Card){n, m};
		}
	}
	d.cards_remaining = 52;
	d.used_cards = 0;
	return d;
}

static int random_number(int modulator){
	return rand() % modulator;
}

void deck_shuffle(Deck* d){
	d->used_cards = 0;
	d->cards_remaining = 52;
}

Card deck_card_draw(Deck* d){
	int r = random_number(d->cards_remaining--);
	int64_t ind = 0;
	while(r > 0 || d->used_cards & ((int64_t)1 << ind)){
		//If card is not used
		if(!(d->used_cards & ((int64_t)1 << ind)))
			r--;
		ind++;
	}
	//Set card to used
	d->used_cards ^= (int64_t)1 << ind;
	return d->cards[ind];
};

int card_to_index(Card c){
	return ((c.rank-2)*4)+c.suit;
}

