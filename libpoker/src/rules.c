#include <stdbool.h>
#include "rules.h"
#include "card_algorithms.h"
#include "hand.h"

Ruleresult create_ruleresult(){
	Ruleresult r;
	r.matches = false;
	r.hand = hand_create();
	return r;
}

static Ruleresult add_kickers(Hand* h, unsigned* bitmask, Ruleresult in){
	int c = 0;
	for(int n = in.hand.count; n < 5; ++n){
		while(c < h->count && ((*bitmask) & (1 << c)) != 0)
			c++;
		if(c == h->count)
			return in;
		hand_card_add(&in.hand, h->cards[c++]);
	}
	return in;
}

Ruleresult flush(Hand* h){
	Ruleresult res = create_ruleresult();
	Hand f = get_cards_with_highest_suit(h);
	if(f.count < 5)
		return res;
	res.matches = true;
	hand_sort(&f);
	res.hand = f;
	res.hand.count = 5;
	return res;
}

Ruleresult x_of_a_kind(Hand* h, unsigned* bitmask, int count){
	unsigned bits = *bitmask;
	Ruleresult res = create_ruleresult();
	int card_number = 0;
	for(; card_number < h->count && res.hand.count < count; ++card_number){
		if((bits & (1 << card_number)) != 0)
			continue;
		//If not the same number as previous card, reset hand
		if(res.hand.count == 0 || h->cards[card_number].rank != res.hand.cards[res.hand.count-1].rank){
			res.hand.count = 0;
			bits = *bitmask;
		}
		bits ^= 1 << card_number;
		hand_card_add(&res.hand, h->cards[card_number]);
		if(res.hand.count == count)
			goto success;
	}
	return res;
success:
	res.matches = true;
	*bitmask = bits;
	return res;
}

Ruleresult four_of_a_kind(Hand* h){
	unsigned bitmask = 0;
	return add_kickers(h, &bitmask, x_of_a_kind(h, &bitmask, 4));
}

Ruleresult three_of_a_kind(Hand* h){
	unsigned bitmask = 0;
	return add_kickers(h, &bitmask, x_of_a_kind(h, &bitmask, 3));
}

Ruleresult one_pair(Hand* h){
	unsigned bitmask = 0;
	return add_kickers(h, &bitmask, x_of_a_kind(h, &bitmask, 2));
}

Ruleresult two_pair(Hand* h){
	unsigned bitmask = 0;
	Ruleresult f = x_of_a_kind(h, &bitmask, 2);
	Ruleresult s = x_of_a_kind(h, &bitmask, 2);
	f.matches = s.matches;
	if(!f.matches)
		return f;
	for(int n = 0; n < s.hand.count; ++n)
		hand_card_add(&f.hand, s.hand.cards[n]);
	return add_kickers(h, &bitmask, f);
}

Ruleresult high_card(Hand* h){
	unsigned bitmask = 0;
	Ruleresult r = create_ruleresult();
	r.matches = true;
	return add_kickers(h, &bitmask, r);
}

Ruleresult full_house(Hand* h){
	unsigned bitmask = 0;
	Ruleresult f = x_of_a_kind(h, &bitmask, 3);
	Ruleresult s = x_of_a_kind(h, &bitmask, 2);
	f.matches = s.matches && f.matches;
	if(!f.matches)
		return f;
	for(int n = 0; n < s.hand.count; ++n)
		hand_card_add(&f.hand, s.hand.cards[n]);
	return f;
}

Ruleresult straight(Hand* h){
	Ruleresult res = create_ruleresult();
	int cards_in_a_row = 0, previous = 16;
	for(int n = 0; n < h->count; ++n){
		Card c = h->cards[n];
		if(c.rank == previous-1){
			cards_in_a_row++,previous--;
			hand_card_add(&res.hand, c);
			//Add ace as lowcard if needed
			if(n == h->count-1 && previous == 2 && h->cards[0].rank == 14
					&& cards_in_a_row == 4){
				cards_in_a_row++;
				hand_card_add(&res.hand, h->cards[0]);
			}
			if(cards_in_a_row >= 5)
				goto success;
		}
		else if(c.rank != previous){
			cards_in_a_row = 1,previous = c.rank;
			res.hand.count = 0;
			hand_card_add(&res.hand, c);
		}
	}
	return res;
success:
	res.matches = true;
	return res;
}

Ruleresult straightflush(Hand* h){
	Hand f = get_cards_with_highest_suit(h);
	return straight(&f);
}
