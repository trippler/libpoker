#include "card.h"
int cards_compare(const void* a, const void* b){
	return ((*(Card*)b).rank - (*(Card*)a).rank);
}

const char* rank_names[] = {"two", "three", "four", "five",
							"six", "seven", "eight", "nine",
							"ten", "jack", "queen", "king",
							"ace"};

const char* rank_to_str(int rank){
	return rank_names[rank-2];
}

const char* suit_names[] = {"spade", "heart", "diamond", "club"};

const char* suit_to_str(Suit s){
	return suit_names[(int)s];
}

