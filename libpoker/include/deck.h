#include <stdint.h>
#include "card.h"
#include "suit.h"
#ifndef DECK_H
#define DECK_H
typedef struct{
	Card cards[52];
	uint64_t used_cards; //64bit mask for cards used;
	int cards_remaining;
}Deck;

Deck deck_create();
void deck_shuffle(Deck* d);
//int random_number(int modulator);
Card deck_card_draw(Deck* d);
int card_to_index(Card c);
#endif
