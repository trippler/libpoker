#ifndef RULES_H
#define RULES_H
#include <stdbool.h>
#include "hand.h"
typedef struct{
	bool matches;
	Hand hand;
}Ruleresult;

Ruleresult straightflush(Hand* h);
Ruleresult four_of_a_kind(Hand* h);
Ruleresult full_house(Hand* h);
Ruleresult flush(Hand* h);
Ruleresult straight(Hand* h);
Ruleresult three_of_a_kind(Hand* h);
Ruleresult two_pair(Hand* h);
Ruleresult one_pair(Hand* h);
Ruleresult high_card(Hand* h);

//Next line only here while testing
//Ruleresult x_of_a_kind(Hand* h, unsigned* bitmask, int count);

#endif

