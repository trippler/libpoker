#ifndef ENGINE_H
#define ENGINE_H
#include "rules.h"
typedef struct{
	int value;
	Ruleresult result;
}Handscore;

typedef enum{
	HIGH_CARD = 1,
	ONE_PAIR = 2,
	TWO_PAIR = 3,
	THREE_OF_A_KIND = 4,
	STRAIGHT = 5,
	FLUSH = 6,
	FULL_HOUSE = 7,
	FOUR_OF_A_KIND = 8,
	STRAIGHTFLUSH = 9
}Handvalues;

Handscore hand_score(Hand* h);
int hands_compare(Hand* a, Hand* b);
#endif
