#ifndef SUIT_H
#define SUIT_H
typedef enum{
	SPADE = 0,
	HEART = 1,
	DIAMOND = 2,
	CLUB = 3
}Suit;
#endif

