#ifndef CARD_H
#define CARD_H
#include "suit.h"
typedef struct{
	unsigned rank:4; //4 bits, 0->15
	Suit suit:2; //2 bits, 0,1,2,3
}Card;

int cards_compare(const void* a, const void* b);
const char* rank_to_str(int rank);
const char* suit_to_str(Suit s);

#endif

