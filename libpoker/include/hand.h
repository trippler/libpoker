#ifndef HAND_H
#define HAND_H
#include "card.h"
typedef struct{
	Card cards[7];
	int count;
}Hand;

Hand hand_create();
void hand_card_add(Hand* h, Card c);
void hand_print(Hand* h);
void hand_sort(Hand* h);
#endif
