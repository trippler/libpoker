#ifndef CARD_ALGORITHMS_H
#define CARD_ALGORITHMS_H
#include "hand.h"

Hand get_cards_with_highest_suit(Hand* h);
#endif

