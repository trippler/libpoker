#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>

#include "hand.h"
#include "deck.h"
#include "card.h"
#include "engine.h"

#define MAX_PLAYERS 9
typedef struct{
	size_t players;
	size_t threads;
	bool verbose;
	size_t simulations;
	char community_cards[2*5+1];
	char player_cards[MAX_PLAYERS][2*2+1];
}Simulation_params;

enum{
	OPTION_NO_TAG = 0,
	OPTION_THREADS = 't',
	OPTION_PLAYERS = 'p',
	OPTION_COMMUNITY_CARDS = 'c',
	OPTION_HELP = 'h',
	OPTION_SIMULATIONS = 's',
	OPTION_UNKNOWN = '?'
};

Simulation_params get_params(int argc, char* argv[]);

typedef struct{
	Hand original, current;
	int wins,id;
}Player;

Player create_player(const char* cards){
	Card c;
	Player p;
	p.wins = 0;
	p.original = hand_create();
	p.id = 0;
	for(int n = 0; n < 4 && cards[n] != 0; ++n){
		char m = cards[n];
		if(m >= '0' && m <= '9')
			c.rank = m - '0';
		else switch(m){
			case 'T':
				c.rank = 10;
				break;
			case 'J':
				c.rank = 11;
				break;
			case 'Q':
				c.rank = 12;
				break;
			case 'K':
				c.rank = 13;
				break;
			case 'A':
				c.rank = 14;
				break;
			case 's':
				c.suit = SPADE;
				break;
			case 'h':
				c.suit = HEART;
				break;
			case 'd':
				c.suit = DIAMOND;
				break;
			case 'c':
				c.suit = CLUB;
				break;
			default:
				printf("Error: %c is not a card property\n", m);
				break;
		}
		if(n % 2 == 1)
			hand_card_add(&p.original, c);
	}
	return p;
}

int player_compare(const void* a, const void* b){
	Player* pa = (Player*)a, *pb = (Player*)b;
	return -hands_compare(&(*pa).current, &(*pb).current);
}

int main(int argc, char* argv[]){
	Simulation_params params = get_params(argc, argv);
	printf("Players: %d, threads: %d\n", (int)params.players, (int)params.threads);
	if(params.community_cards[0] != 0){
		printf("Table cards: %s\n", params.community_cards);
	}
	assert(params.players >= 2);
	Player players[params.players];
	Deck deck = deck_create();
	for(int n = 0; n < params.players; ++n){
		players[n] = create_player(params.player_cards[n]);
		players[n].id = n;
		printf("Player %d hand: ", n);
		hand_print(&players[n].original);
		printf("\n");
		for(int m = 0; m < players[n].original.count; ++m)
			deck.used_cards ^= card_to_index(players[n].original.cards[m]);
	}
	int ties = 0;
	for(int n = 0; n < params.simulations; ++n){
		Deck d = deck;
		for(int m = 0; m < params.players; ++m){
			players[m].current = players[m].original;
			while(players[m].current.count < 2)
				hand_card_add(&players[m].current, deck_card_draw(&d));
		}
		for(int m = 0; m < 5; ++m){
			Card c = deck_card_draw(&d);
			for(int i = 0; i < params.players; ++i)
				hand_card_add(&players[i].current, c);
		}
		qsort(players, params.players, sizeof(Player), player_compare);
		int score = hands_compare(&players[0].current, &players[1].current);
		//if(score < 0)
		//	players[1].wins++;
		assert(score >= 0);
		if(score > 0)
			players[0].wins++;
		if(score == 0)
			ties++;
	}
	for(int n = 0; n < params.players; ++n){
		printf("Player %d wins: %d (%f%%)\n", players[n].id, players[n].wins, 
			((double)players[n].wins*100.0)/(double)params.simulations);
	}
	printf("Ties: %d (%f%%)\n", ties, 
		((double)ties*100.0)/(double)params.simulations);
}

void parse_argument(Simulation_params* params, int argument, int option_index,
			struct option long_options[]){
	if(argument > 0 && 1 == 2)
		printf("Got -%c with \"%s\"\n", (char)argument, (optarg)?optarg : "NULL");
	switch (argument){
		case OPTION_NO_TAG:
			if(long_options[option_index].flag != 0){
				//Flag was set, go to next option
				break;
			}
			printf("Option \"%s\" with argument %s\n", 
			long_options[option_index].name, (optarg)?optarg : "NULL");
			break;
		case OPTION_THREADS:
			assert(optarg != NULL);
			params->threads = atoi(optarg);
			break;
		case OPTION_PLAYERS:
			assert(optarg != NULL);
			params->players = atoi(optarg);
			break;
		case OPTION_COMMUNITY_CARDS:
			assert(optarg != NULL);
			assert(strlen(optarg) <= 10);
			memcpy(params->community_cards, optarg, strlen(optarg));
			break;
		case OPTION_HELP:
			printf("This is where you'd get help if there was any..\n");
			exit(EXIT_SUCCESS);
			break;
		case OPTION_SIMULATIONS:
			params->simulations = atoi(optarg);
			break;
		case OPTION_UNKNOWN:
			//printf("Got unknown option with \"%s\"\n", (optarg)?optarg : "NULL");
			break;
		default:
			abort();
	}
}

Simulation_params create_simulation_params(){
	Simulation_params p;
	p.threads = 1;
	p.players = 2;
	p.verbose = false;
	p.simulations = 0x10000;
	memset(p.community_cards, 0, sizeof(p.community_cards));
	memset(p.player_cards, 0, sizeof(p.player_cards));
	return p;
}

Simulation_params get_params(int argc, char* argv[]){
	Simulation_params params = create_simulation_params();
	static int verbose = 0;
	static struct option long_options[] =
	{
		{"verbose", no_argument, &verbose, 1},
		{"threads", required_argument, 0, OPTION_THREADS},
		{"players", required_argument, 0, OPTION_PLAYERS},
		{"community", required_argument, 0, OPTION_COMMUNITY_CARDS},
		{"help", no_argument, 0, OPTION_HELP},
		{"simulations", required_argument, 0, OPTION_SIMULATIONS},
		{0, 0, 0, 0}
	};
	int opt_ind = 0;
	int c = 0;
	while((c = getopt_long(argc, argv, "t:p:", long_options, &opt_ind)) != -1){
		parse_argument(&params, c, opt_ind, long_options);
	}
	//printf("Verbose is %s\n", (verbose)? "true" : "false");
	int player_number = 0;
	while(optind < argc){
		int length = strlen(argv[optind]);
		if(length <= 4)
			memcpy(params.player_cards[player_number++], argv[optind], length);
		optind++;
	}
	params.verbose = (verbose != 0);
	return params;
}
