all: library main_driver

clean: clean_lib clean_driver
	rm bin/*

library:
	cd libpoker && make && cd -
	cp libpoker/lib/libpoker.so bin/

main_driver:
	cd driver && make && cd -

clean_lib:
	cd libpoker && make clean && cd -

clean_driver:
	cd driver && make clean && cd -
